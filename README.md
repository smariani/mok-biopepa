# README #

This repository contains the codebase necessary to replicate the biochemical coordination related experiments discussed in papers:

 * *On the "Local-to-Global" Issue in Self-Organisation: Chemical Reactions with Custom Kinetic Rates* (doi: 10.1109/SASOW.2014.14)
 * *Parameter Engineering vs. Parameter Tuning: the Case of Biochemical Coordination in MoK* (url: <http://ceur-ws.org/Vol-1099/paper5.pdf>)
 * *Analysis of the Molecules of Knowledge Model with the BioPepa Eclipse Plugin* (doi: 10.6092/unibo/amsacta/3783)

It is strongly suggested to read those papers and therein cited related works to get a reasonable understanding of the experiments purpose.

### How do I get set up? ###

* Install Eclipse IDE (<https://eclipse.org>)
* Install BioPEPA Eclipse plugin (<http://www.biopepa.org>)
* Clone codebase and import in Eclipse (if you don't know how to use Git go [here](https://git-scm.com/doc))
* Run the experiments according BioPEPA workflow (<http://www.biopepa.org>)

### Contribution welcome ###

* Add tests
* Review code
* Suggest improvements

### Who do I talk to? ###

* Repo owner