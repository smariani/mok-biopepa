// topology definition
location sports : size = 1, type = compartment;
location economics : size = 1, type = compartment;
location crime : size = 1, type = compartment;
location politics : size = 1, type = compartment;

// saturation & time weights
SATi = 0.5;
TIMEi = 1 - SATi;
// diffusion weight
DW = 0.75;

// injection functional rates (sources => atoms)
injS = [H(ss@sports - as@sports) * (TIMEi * (1 / (1 + time)) + SATi * (ss@sports - as@sports))];
injE = [H(se@economics - ae@economics) * (TIMEi * (1 / (1 + time)) + SATi * (se@economics - ae@economics))];
injC = [H(sc@crime - ac@crime) * (TIMEi * (1 / (1 + time)) + SATi * (sc@crime - ac@crime))];
injP = [H(sp@politics - ap@politics) * (TIMEi * (1 / (1 + time)) + SATi * (sp@politics - ap@politics))];

// diffusion functional rates (a@x => a@y)
diffSE = [DW * as@sports - as@economics];
diffSC = [DW/2 * as@sports - as@crime];
diffSP = [DW/3 * as@sports - as@politics];
diffES = [DW * ae@economics - ae@sports];
diffEC = [DW * ae@economics - ae@crime];
diffEP = [DW * ae@economics - ae@politics];
diffCS = [DW * ac@crime - ac@sports];
diffCE = [DW * ac@crime - ac@economics];
diffCP = [DW * ac@crime - ac@politics];
diffPS = [DW * ap@politics - ap@sports];
diffPE = [DW * ap@politics - ap@economics];
diffPC = [DW * ap@politics - ap@crime];

// sources inject atoms
ss = 	(injS, 1) (+) ss@sports;
se = 	(injE, 1) (+) se@economics;
sc = 	(injC, 1) (+) sc@crime;
sp = 	(injP, 1) (+) sp@politics;

// atoms are produced from sources (inj) and can diffuse to other compartments (diff)
as = 	(injS, 1) >> as@sports	+ (diffSE[sports->economics], 1) (.) as
								+ (diffSC[sports->crime], 1) (.) as
								+ (diffSP[sports->politics], 1) (.) as;
ae = 	(injE, 1) >> ae@economics	+ (diffES[economics->sports], 1) (.) ae
									+ (diffEC[economics->crime], 1) (.) ae
									+ (diffEP[economics->politics], 1) (.) ae;
ac = 	(injC, 1) >> ac@crime	+ (diffCS[crime->sports], 1) (.) ac
								+ (diffCE[crime->economics], 1) (.) ac
								+ (diffCP[crime->politics], 1) (.) ac;
ap = 	(injP, 1) >> ap@politics	+ (diffPS[politics->sports], 1) (.) ap
									+ (diffPE[politics->economics], 1) (.) ap
									+ (diffPC[politics->crime], 1) (.) ap;

// System initial conditions
ss@sports[1000] <*> se@economics[750] <*> sc@crime[500] <*> sp@politics[250] <*>
	as@sports[0] <*> as@economics[0] <*> as@crime[0] <*> as@politics[0] <*>
	ae@economics[0] <*> ae@sports[0] <*> ae@crime[0] <*> ae@politics[0] <*>
	ac@crime[0] <*> ac@sports[0] <*> ac@economics[0] <*> ac@politics[0] <*>
	ap@politics[0] <*> ap@sports[0] <*> ap@economics[0] <*> ap@crime[0]